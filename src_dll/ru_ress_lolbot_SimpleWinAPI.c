#include "ru_ress_lolbot_SimpleWinAPI.h"
#include <windows.h>
#include <string.h>

JNIEXPORT jlong JNICALL Java_ru_ress_lolbot_SimpleWinAPI_findWindow
  (JNIEnv *env, jclass class, jstring title) {
	const char *name = (*env)->GetStringUTFChars( env, title , NULL );
	HWND h = FindWindow(NULL, TEXT(name));
	return (long)h;
}

JNIEXPORT jint JNICALL Java_ru_ress_lolbot_SimpleWinAPI_getClientX
  (JNIEnv *env, jclass class, jlong hwd) {
	RECT rect;
	GetWindowRect((long)hwd, &rect);
	return rect.left;
}

JNIEXPORT jint JNICALL Java_ru_ress_lolbot_SimpleWinAPI_getClientY
  (JNIEnv *env, jclass class, jlong hwd) {
	RECT rect;
	GetWindowRect((long)hwd, &rect);
	return rect.top;
}

JNIEXPORT jint JNICALL Java_ru_ress_lolbot_SimpleWinAPI_getClientWidth
  (JNIEnv *env, jclass class, jlong hwd) {
	RECT rect;
	GetWindowRect((long)hwd, &rect);
	return rect.right - rect.left;
}

JNIEXPORT jint JNICALL Java_ru_ress_lolbot_SimpleWinAPI_getClientHeight
  (JNIEnv *env, jclass class, jlong hwd) {
	RECT rect;
	GetWindowRect((long)hwd, &rect);
	return rect.bottom - rect.top;
}
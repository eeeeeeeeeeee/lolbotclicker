package ru.ress.lolbot;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class MenuForm {
    public JPanel panel1;
    public JFrame selfFrame;

    private JTextField textField1;
    private JButton selectButton;
    private JTextArea textArea1;
    private JSpinner spinner1;
    private JButton startButton;
    private JScrollPane scrollPane;
    private JSpinner spinner2;
    private JButton stopButton;
    private JCheckBox logsCheckBox;
    private JSpinner spinnerLogs;

    private String path;
    private BotThread botThread;

    public void initFrame(JFrame frame) {
        selfFrame = frame;
        frame.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {

            }

            @Override
            public void windowClosing(WindowEvent e) {
                if(botThread != null) {
                    botThread.shitHappen = true;
                    while(botThread.isAlive());
                }
                selfFrame.dispose();
            }

            @Override
            public void windowClosed(WindowEvent e) {

            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        });
    }

    private void createUIComponents() {
        spinner2 = new JSpinner(new SpinnerNumberModel(1, 0, 5, 1));
        spinnerLogs = new JSpinner(new SpinnerNumberModel(3, 1, 10, 1));
    }

    class BotThread extends Thread {
        public boolean shitHappen = false;

        public BotThread() {
            super();
            setDaemon(true);
            stopButton.setEnabled(true);
        }

        @Override
        public void run() {
            boolean wrLogs = false;
            if(logsCheckBox.isSelected()) wrLogs = true;

            ScreenSaver saver = null;
            if(wrLogs) {
                saver = new ScreenSaver((int)spinnerLogs.getValue());
                saver.start();
            }

            GameScript gameScript = null;
            try {
                while ((int) spinner1.getValue() > 0 && !shitHappen) {
                    spinner1.setValue((int) spinner1.getValue() - 1);
                    gameScript = new GameScript((int)spinner2.getValue());
                    Logger log = gameScript.getLog();
                    gameScript.start();

                    while (gameScript.isAlive()) {
                        while (log.size() > 0) {
                            String str = log.pop();
                            textArea1.insert(str + "\n", 0);
                        }
                        sleep(1000);
                        if (textArea1.getLineCount() > 100) {
                            if(wrLogs) writeLogs();
                            textArea1.setText("Cleared");
                        }
                        if(shitHappen) {
                            gameScript.close();
                            gameScript.interrupt();
                        }
                    }

                    while (log.size() > 0) {
                        String str = log.pop();
                        textArea1.insert(str + "\n", 0);
                    }
                }
            } catch (InterruptedException | AWTException e) {
                e.printStackTrace();
                if(gameScript != null) {
                    gameScript.interrupt();
                }
                spinner1.setValue(0);
            }

            if(wrLogs) {
                writeLogs();
                saver.close();
            }

            spinner1.setValue(0);
            startButton.setEnabled(true);
            replace("resources/back.cfg", getGamePath());
            replace("resources/backsettings.json", getSettingsPath());
            stopButton.setEnabled(false);
        }
    }

    private void writeLogs() {
        DateFormat format = new SimpleDateFormat("dd_MM_HH_mm_ss");
        try {
            Date date = new Date();
            File file = new File("pictures"+File.separator+format.format(date)+".txt");

            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(textArea1.getText());
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getGamePath() {
        return path + File.separator + "Config" + File.separator + "game.cfg";
    }

    private String getSettingsPath() {
        return path + File.separator + "Config" + File.separator + "PersistedSettings.json";
    }

    private boolean checkPath(String str) {
        String dir = str + File.separator + "Config" + File.separator + "game.cfg";
        System.out.println(dir);
        File file = new File(dir);
        if(file.exists()) {
            return true;
        }
        return false;
    }

    private void replace(String from, String to) {
        StringBuilder def = new StringBuilder();

        try {
            FileReader fileReader = new FileReader(from);
            Scanner in = new Scanner(fileReader);

            while(in.hasNextLine()) {
                String line = in.nextLine();
                def.append(line).append("\n");
            }

            String defCopy = def.toString();
            FileWriter fileWriter = new FileWriter(to);
            fileWriter.write(defCopy);
            fileWriter.close();


        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public MenuForm() {
        selectButton.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Выберите папку с League of Legends...");
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            if(fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                if(checkPath(fileChooser.getSelectedFile().toString())) {
                    path = fileChooser.getSelectedFile().toString();
                    textField1.setText(path);
                    startButton.setEnabled(true);
                    selectButton.setEnabled(false);

                    try {
                        // Запись конфига
                        File file = new File("botcfg.cfg");
                        FileWriter fileWriter = new FileWriter(file);
                        fileWriter.write(path);
                        fileWriter.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    return;
                }

                JOptionPane.showMessageDialog(null, "Некорректная директория. Не найдена папка Config",
                        "Ошибка", JOptionPane.ERROR_MESSAGE);

            }
        });
        startButton.addActionListener(e -> {
            startButton.setEnabled(false);
            //Копирование файла
            replace(getGamePath(), "resources/back.cfg");
            replace(getSettingsPath(), "resources/backsettings.json");
            replace("resources/game.cfg", getGamePath());
            replace("resources/PersistedSettings.json", getSettingsPath());
            botThread = new BotThread();
            botThread.start();




        });
        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                botThread.shitHappen = true;
            }
        });

        try {

            // Чтение конфига
            File file = new File("botcfg.cfg");
            if (file.exists()) {
                FileReader fileReader = new FileReader(file);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                path = bufferedReader.readLine();
                fileReader.close();

                startButton.setEnabled(true);
                textField1.setText(path);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

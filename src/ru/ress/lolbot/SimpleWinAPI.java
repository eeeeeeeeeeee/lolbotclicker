package ru.ress.lolbot;

public class SimpleWinAPI {
    static native long findWindow(String title);
    //static native void toTop(long wnd);
    static native int getClientX(long wnd);
    static native int getClientY(long wnd);
    static native int getClientWidth(long wnd);
    static native int getClientHeight(long wnd);
    //static native int move(long wnd);
}

package ru.ress.lolbot;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ScreenSaver extends Thread {
    private int delay = 3000*60;

    public ScreenSaver(int min) {
        delay = min*1000*60;
    }

    public void close() {
        interrupt();
    }

    @Override
    public void run() {
        Rectangle  desktop = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
        DateFormat format = new SimpleDateFormat("dd_MM_HH_mm_ss");

        try {
            Robot robot = new Robot();
            long time = System.currentTimeMillis();
            while(!isInterrupted()) {
                while(System.currentTimeMillis() < time + delay);

                time = System.currentTimeMillis();

                Date date = new Date();

                BufferedImage capture = robot.createScreenCapture(desktop);
                ImageIO.write(capture, "PNG", new File("pictures"+File.separator+format.format(date)+".png"));
            }
        } catch (AWTException | IOException e) {
            e.printStackTrace();
        }
    }
}

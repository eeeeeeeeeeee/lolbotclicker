package ru.ress.lolbot;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Logger {
    private ArrayList<String> log;

    public Logger() {
        log = new ArrayList<>();
        log.add("Log system init");
    }

    public void add(String info) {
        Date date = new Date();
        DateFormat dateFormat = DateFormat.getTimeInstance();
        String dateString = dateFormat.format(date);

        String stringBuilder = "[" +
                dateString +
                "] " +
                info;
        log.add(stringBuilder);
    }

    public int size() {
        return log.size();
    }

    public String pop() {
        if(log.size() < 1) {
            return null;
        }

        String info = log.get(0);
        log.remove(0);
        return info;
    }
}

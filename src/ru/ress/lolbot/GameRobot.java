package ru.ress.lolbot;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;

public class GameRobot extends Robot {

    public GameRobot() throws AWTException {
    }

    public void mouseClick(int x, int y, int key) {
        mouseMove(x, y);
        delay(10);
        mousePress(key);
        delay(10);
        mouseRelease(key);
        delay(10);
    }

    public void castSpell() {
        int random = (int)(Math.random()*5);
        int key = KeyEvent.VK_Q;

        switch (random) {
            case 1: {
                key = KeyEvent.VK_W;
                break;
            }
            case 2: {
                key = KeyEvent.VK_E;
                break;
            }
            case 3: {
                key = KeyEvent.VK_R;
                break;
            }
        }

        delay(10);

        keyPress(key);
        delay(10);
        keyRelease(key);
    }

    public void learnSpell() {
        keyPress(KeyEvent.VK_CONTROL);
        delay(10);

        castSpell();

        delay(10);

        keyRelease(KeyEvent.VK_CONTROL);
    }

    public double comparePictures(BufferedImage img0, BufferedImage img1, Rectangle rec0, Rectangle rec1) {
        int real = 0;
        int potencial = rec0.width*rec0.height;
        for(int i=0;i<rec0.width;i++) {
            for(int j=0;j<rec0.height;j++) {
                Color[] color = new Color[2];
                color[0] = new Color(img0.getRGB(rec0.x+i,rec0.y+j));
                color[1] = new Color(img1.getRGB(rec1.x+i,rec1.y+j));

                double comparing = 0;
                if(Math.abs(color[0].getRed() - color[1].getRed()) < 5) comparing += 1;
                if(Math.abs(color[0].getBlue() - color[1].getBlue()) < 5) comparing += 1;
                if(Math.abs(color[0].getGreen() - color[1].getGreen()) < 5) comparing += 1;

                if(comparing > 2) real += 1;


                //if(img0.getRGB(rec0.x+i,rec0.y+j) == img1.getRGB(rec1.x+i,rec1.y+j)) real += 1;
            }
        }
        return ((double) real)/potencial;
    }

}

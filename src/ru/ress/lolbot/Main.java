package ru.ress.lolbot;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class Main {
    static {
        System.out.println(new File("").getAbsolutePath());
        boolean is64bit = false;
        if (System.getProperty("os.name").contains("Windows")) {
            is64bit = (System.getenv("ProgramFiles(x86)") != null);
        } else {
            is64bit = (System.getProperty("os.arch").contains("64"));
        }
        if(is64bit) System.load(new File("SimpleWinAPI64.dll").getAbsolutePath());
        else System.load(new File("SimpleWinAPI32.dll").getAbsolutePath());
    }

    public static void main(String[] args) throws AWTException, IOException {
        JFrame frame = new JFrame();

        MenuForm form = new MenuForm();
        form.initFrame(frame);

        frame.setContentPane(form.panel1);
        frame.pack();
        frame.setVisible(true);
    }
}

package ru.ress.lolbot;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class GameScript extends Thread {
    private boolean running = true;
    private Logger logger;
    private GameRobot robot;

    private int clientX, clientY;
    private int x, y;
    private Rectangle desktop;
    private int preferChamp;

    public GameScript(int preferChamp) throws AWTException {
        robot = new GameRobot();
        setDaemon(true);
        logger = new Logger();

        desktop = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
        x = (desktop.width-1024)/2;
        y = (desktop.height-500)/2;

        long wnd = SimpleWinAPI.findWindow("League of Legends");
        clientX = SimpleWinAPI.getClientX(wnd);
        clientY = SimpleWinAPI.getClientY(wnd);

        if(wnd == 0) {
            logger.add("No client; WND is "+Long.toString(wnd));
            running = false;
        } else {
            logger.add("Client WND is "+Long.toString(wnd)+"X: "+clientX+" Y: "+clientY);
        }
        this.preferChamp = preferChamp;
    }

    public void close() {
        running = false;
        logger.add("Closing");
    }

    public Logger getLog() {
        return logger;
    }

    private void gameloop() throws IOException {
        if(!running) return;
        long buytime = System.currentTimeMillis();
        Rectangle rect = new Rectangle((desktop.width-1024)/2, (desktop.height-500)/2, 1024, 500);


        //================================= ГЕЙМПЛЕЙ КЕК ==========
        BufferedImage imgEnd = ImageIO.read(new File("resources/min3.png"));
        Rectangle recFinish = new Rectangle(487,292,54,11);

        BufferedImage imgHeal = ImageIO.read(new File("resources/min1.png"));
        Rectangle recHeal = new Rectangle(438,479,20,4);

        long time = System.currentTimeMillis();
        long ctime = System.currentTimeMillis();

        while(true) {
            if(!running) {
                return;
            }

            robot.delay(300);
            robot.learnSpell();
            robot.delay(500);

            BufferedImage capture = robot.createScreenCapture(rect);
            double fFinish = robot.comparePictures(imgEnd, capture, recFinish, recFinish);

            if(fFinish>0.7) {
                robot.mouseClick(x+490, y+300, InputEvent.BUTTON1_MASK);
                robot.delay(30000);
                break;
            }

            double fNeedHeal = robot.comparePictures(imgHeal, capture, recHeal, recHeal);
            if(fNeedHeal < 0.8) {
                robot.keyPress(KeyEvent.VK_F);
                robot.delay(100);
                robot.keyRelease(KeyEvent.VK_F);
                robot.delay(10);
            }


            if(System.currentTimeMillis()-buytime < 12000) {
                continue;
            }

            if(System.currentTimeMillis()-ctime<6000) {
                if(Math.random()>0.9) {
                    robot.keyPress(KeyEvent.VK_SPACE);
                    robot.delay(100);
                    robot.keyRelease(KeyEvent.VK_SPACE);
                    robot.delay(100);

                    robot.mouseMove(x + 557, y + 185);

                    robot.castSpell();
                    robot.delay(100);
                }

                robot.mouseMove(x + 1008, y + 389);
                robot.keyPress(KeyEvent.VK_A);
                robot.keyRelease(KeyEvent.VK_A);
                robot.delay(100);
                robot.mouseClick(x + 1008, y + 389, InputEvent.BUTTON1_MASK);
                robot.delay(10);


            } else {

                robot.mouseClick(x + 942, y + 453, InputEvent.BUTTON3_MASK);
                robot.delay(1300);
                ctime = System.currentTimeMillis();

                if(System.currentTimeMillis()-time > 60*1000*5) {
                    robot.keyPress(KeyEvent.VK_B);
                    robot.delay(10);
                    robot.keyRelease(KeyEvent.VK_B);
                    time = System.currentTimeMillis();
                    buytime = System.currentTimeMillis();
                    continue;
                }

                if(fNeedHeal < 0.8) {
                    robot.keyPress(KeyEvent.VK_2);
                    robot.delay(100);
                    robot.keyRelease(KeyEvent.VK_2);
                }
            }

        }




        robot.mouseClick(clientX+378, clientY+238, InputEvent.BUTTON1_MASK);
        robot.delay(20000);

        // Принятие наград
        BufferedImage imgOk = ImageIO.read(new File("resources/e15.png"));
        Rectangle recOk = new Rectangle(458,528,108,32);
        rect = new Rectangle(clientX, clientY, 1024, 576);


        while(true) {
            if(!running) {
                return;
            }

            robot.delay(4000);
            BufferedImage capture = robot.createScreenCapture(rect);
            double fOk = robot.comparePictures(imgOk, capture, recOk, recOk);
            if(fOk < 0.9) break;

            logger.add("Getting reward");

            robot.mouseClick(clientX+487, clientY+539, InputEvent.BUTTON1_MASK);
            robot.delay(1000);
            robot.mouseMove(0, 0);
            robot.delay(1000);
        }

        logger.add("All rewards has been get");

        robot.delay(10000);
        robot.mouseClick(clientX+357, clientY+546, InputEvent.BUTTON1_MASK);

        logger.add("Finish cycle. Go next");
    }


    private boolean checkHero(int i, BufferedImage imgSelect) {
        Rectangle rect = new Rectangle(clientX, clientY, 1024, 576);
        Rectangle recSelect = new Rectangle(469,479,564-469,493-479);

        robot.mouseClick(clientX+307+i*80, clientY+137, InputEvent.BUTTON1_MASK);
        robot.delay(3000);

        BufferedImage capture = robot.createScreenCapture(rect);
        double factor = robot.comparePictures(imgSelect, capture, recSelect, recSelect);
        logger.add("Select hero factor "+Double.toString(factor));

        if(factor>0.8) {
            robot.mouseClick(clientX+490, clientY+485, InputEvent.BUTTON1_MASK);
            return true;
        }

        return false;
    }

    private void pickHero() throws IOException {
        if(!running) return;
        Rectangle rect = new Rectangle(clientX, clientY, 1024, 576);

        robot.delay(500);

        BufferedImage imgSelect = ImageIO.read(new File("resources/er8.png"));
        Rectangle recSelect = new Rectangle(469,479,564-469,493-479);

        // Выбор адк
        robot.mouseClick(clientX+384, clientY+80, InputEvent.BUTTON1_MASK);

        robot.delay(1000);

        // Попытка взять рандомного адк
        checkHero(preferChamp, imgSelect);

        robot.delay(2000);

        // Перебор адк

        for(int i=0;i<6;i++) {
            if(!running) break;

            if(checkHero(i,imgSelect)) break;

            robot.delay(2000);
        }
    }


    private void findMatch() throws IOException {
        Rectangle rect = new Rectangle(clientX, clientY, 1024, 576);

        robot.delay(500);

        BufferedImage imgAccept = ImageIO.read(new File("resources/er6.png"));
        BufferedImage imgLobby = ImageIO.read(new File("resources/er7.png"));

        Rectangle recAccept = new Rectangle(450,434,567-450,456-434);
        Rectangle recLobby = new Rectangle(283,78,300-283,93-78);

        while(true) {
            if(!running) {
                return;
            }

            BufferedImage capture = robot.createScreenCapture(rect);
            double fAccept = robot.comparePictures(imgAccept, capture, recAccept, recAccept);
            double fLobbby = robot.comparePictures(imgLobby, capture, recLobby, recLobby);
            logger.add("Lobby chance: "+Double.toString(fLobbby)+"; Accept chance: "+Double.toString(fAccept));

            robot.delay(500);
            if(fAccept>0.8) {
                robot.mouseClick(clientX+482, clientY+444, InputEvent.BUTTON1_MASK);
                robot.delay(1000);
                robot.mouseMove(0, 0);
            }

            if(fLobbby>0.6) {
                logger.add("Choosing hero");
                break;
            }
        }
    }

    private void startGame() throws IOException {
        if(!running) return;
        Rectangle rect = new Rectangle(clientX, clientY, 1024, 576);

        BufferedImage img = ImageIO.read(new File("resources/er3.png"));
        Rectangle rectangle = new Rectangle(62,24,55,17);

        while(true) {
            if(!running) {
                return;
            }
            BufferedImage capture = robot.createScreenCapture(rect);

            double factor = robot.comparePictures(img, capture, rectangle, rectangle);
            logger.add("Begin button chance: "+Double.toString(factor));

            if(factor>0.4) {
                robot.delay(2000);

                robot.mouseClick(clientX+93, clientY+33, InputEvent.BUTTON1_MASK);
                robot.delay(2000);

                robot.mouseClick(clientX+113, clientY+78, InputEvent.BUTTON1_MASK);
                robot.delay(2000);

                robot.mouseClick(clientX+268, clientY+399, InputEvent.BUTTON1_MASK);
                robot.delay(4000);

                robot.mouseClick(clientX+413, clientY+550, InputEvent.BUTTON1_MASK);
                robot.delay(4000);

                robot.mouseClick(clientX+413, clientY+550, InputEvent.BUTTON1_MASK);
                logger.add("Finding a match");
                break;
            }

            robot.delay(1000);
        }
    }

    private void startScript() throws AWTException, IOException, InterruptedException {
        if(!running) return;
        logger.add("Loop start");
        startGame();

        // ======================= ПОИСК МАТЧА ===================================

        findMatch();

        // ======================= ПИК ГЕРОЯ ===================================

        pickHero();

        // ======================= ФАЗА СТАРТА ===================================

        BufferedImage imgStart = ImageIO.read(new File("resources/min1.png"));
        Rectangle recStart = new Rectangle(588,484,26,16);


        BufferedImage imgAccept = ImageIO.read(new File("resources/er6.png"));
        Rectangle recAccept = new Rectangle(450,434,567-450,456-434);

        /*BufferedImage imgCam = ImageIO.read(new File("resources/min_cam.png"));
        Rectangle recCam = new Rectangle(872,490,10,8);*/
        while(true) {
            if(!running) {
                return;
            }

            Rectangle rect = new Rectangle((desktop.width-1024)/2, (desktop.height-500)/2, 1024, 500);
            BufferedImage capture = robot.createScreenCapture(rect);

            double factor = robot.comparePictures(imgStart, capture, recStart, recStart);
            BufferedImage capture_accept = robot.createScreenCapture(new Rectangle(clientX, clientY, 1024, 576));
            double fAccept = robot.comparePictures(imgAccept, capture_accept, recAccept, recAccept);

            logger.add("Start game client chance: "+Double.toString(factor)+"; Accept button chance: "+Double.toString(fAccept));

            if(fAccept > 0.9) {
                findMatch();
                pickHero();
            }


            if(factor > 0.9) {
                logger.add("Game has been started");

                // ЗАКУПКА =========================
                robot.mouseClick(x+500, y+200, InputEvent.BUTTON1_MASK);

                robot.delay(2000);
                robot.mouseClick(x+500, y+210, InputEvent.BUTTON1_MASK);

                robot.delay(1000);

               // ПОКУПКА СТАРТОВЫХ ИТЕМОВ

                /*robot.mouseClick(x+636, y+491, InputEvent.BUTTON1_MASK);
                robot.delay(3000);

                robot.mouseClick(x+267, y+67, InputEvent.BUTTON1_MASK);
                robot.delay(3000);

                robot.mouseClick(x+145, y+141, InputEvent.BUTTON1_MASK);
                robot.delay(3000);


                robot.mouseClick(x+326, y+252, InputEvent.BUTTON3_MASK);
                robot.delay(2000);

                robot.mouseClick(x+288, y+116, InputEvent.BUTTON3_MASK);
                robot.delay(2000);

                robot.mouseClick(x+619, y+48, InputEvent.BUTTON1_MASK);
                robot.delay(2000);*/

                // Lock camera

                //double camFactor = comparePictures(imgCam, capture, recCam, recCam);
                //if(camFactor>0.9) {
                    robot.delay(500);
                    /*robot.keyPress(KeyEvent.VK_Y);
                    robot.delay(10);
                    robot.keyRelease(KeyEvent.VK_Y);*/

                    robot.mouseMove(x+619,y+48);
                //}


                //START
                logger.add("I will do something after 40 sec");
                robot.delay(40000);

                logger.add("Alive and shopped");
                break;
            }

            // ЕСЛИ ЗАБАГУЕТСЯ КЕК
            robot.mouseClick(x+687, y+312, InputEvent.BUTTON1_MASK);
            robot.delay(2000);

            robot.mouseClick(x+405, y+228, InputEvent.BUTTON1_MASK);
            robot.delay(1000);
        }

        gameloop();
    }


    @Override
    public void run() {
        try {
            startScript();
        } catch (AWTException | IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}

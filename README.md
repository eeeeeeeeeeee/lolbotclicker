# Bot-autoclicker for League of Legends
Bot for raise account's level / playing low-priority. Бот для прокачки аккаунта / снятия лоу-приорити. 
Working only with russian client. Актуален только для русскоязычного клиента.

You can set russion language in EU West client by this program: [Region Changer].
Вы можете установить русскоязычный клиент для EU West региона при помощи программы, ссылка на которую предоставлена выше.

# Инструкция
1. Скачайте последнюю собранную версию программы: [LoL Bot-Autoclicker] (или соберите самостоятельно из исходников).
2. Запустите League of Legends. Убедитесь, что вы находитесь на главном экране русскоязычного клиента.
![Пример экрана](https://gitlab.com/eeeeeeeeeeee/lolbotclicker/raw/master/pictures/p2.png)
3. Запустите Bot-Autoclicker. Нажмите кнопку "Выбрать". В появившемся меню выберите папку, в которую вы установили League of Legends и нажмите "Open".
![Пример](https://gitlab.com/eeeeeeeeeeee/lolbotclicker/raw/master/pictures/p0.png)
4. Выберите количество повторов (От 1 и выше) и номер героя, которого будет пикать бот (От 0 до 5. Рекомендуется не ставить высокое значение, а также купить пару дешевых адк типа Сивир или Эш).
![Пример экрана](https://gitlab.com/eeeeeeeeeeee/lolbotclicker/raw/master/pictures/p1.png)
5. Нажмите "Старт"
6. Разверните клиент с игрой (Но ни в коем случае не перемещайте его по экрану, так как бот уже зафиксировал его положение и работает исключительно с теми координатами, что у него есть).
7. Нажмите "Стоп", чтобы завершить работу бота. (На данный момент никаких хот-кеев не предусмотренно)

# Пример игр, сыгранных ботом
![Пример экрана](https://gitlab.com/eeeeeeeeeeee/lolbotclicker/raw/master/pictures/p3.png)

# А меня не забанят за бота?
Ну лично у меня бот сыграл около 200 игр и прокачал один аккаунт на EU West до 30 лвл. Плюс я уже несколько раз успел снять ботом лоу-приорити. Бана никакого не прилетело, сейчас спокойно играю в ранкед. 

   [Region Changer]: <https://yadi.sk/d/ZyKaSML_Se2tHA>
   [LoL Bot-Autoclicker]: <https://yadi.sk/d/tXvHyawgsfFX-Q>